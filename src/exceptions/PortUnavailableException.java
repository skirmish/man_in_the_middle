package exceptions;

public class PortUnavailableException extends RuntimeException {

	private static final long serialVersionUID = 4483570871075656837L;

	public PortUnavailableException() {
		super();
		
	}

	public PortUnavailableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public PortUnavailableException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public PortUnavailableException(String message) {
		super(message);
		
	}

	public PortUnavailableException(Throwable cause) {
		super(cause);
		
	}

}
