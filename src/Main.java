import javax.swing.JOptionPane;

import exceptions.InvalidPortException;

public class Main {
	public static void main(String[] args) {
		int mitmPort = askPort("Man in the middle");
		if(mitmPort ==0)System.exit(0);
		String address = Utils.askAddress();
		if(address == null)System.exit(0);
		int serverPort = askPort("Server");
		if(serverPort ==0)System.exit(0);
		try{
			ManInTheMiddle mitm = new ManInTheMiddle(mitmPort, "localhost", serverPort);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
	}
	
	private static int askPort(String portstr){
		int port = 0;
		while(port == 0){
			String portStr = JOptionPane.showInputDialog(null, portstr+" port: ", "SKIRMISH | SERVER", JOptionPane.QUESTION_MESSAGE);
			if(portStr == null){
				return 0;
			}else{
				try{
					port = Validate.port(portStr);
				}catch(InvalidPortException ipe){
					Utils.popupError(ipe.getMessage());
					port = 0;
				}
			}
		}
		return port;
	}
}
