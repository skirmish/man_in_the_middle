import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import exceptions.InvalidServerAddressException;
import exceptions.PacketReceiveException;
import exceptions.PortUnavailableException;

public class ManInTheMiddle {
	/* SOCKET PROGRAMMING ATTRIBUTES */
	private static final int MAXIMUM_BUFFER_SIZE = 1024;
	private final DatagramSocket serverSocket;
	private volatile boolean alive;
	private Thread clientListener;
	private InetAddress serverAddress;
	private int serverPort;
	
	/* GUI ATTRIBUTES*/
	private final MitMUI mitmUI;
	
	public ManInTheMiddle(int port, String serverAddress, int serverPort){
		try {
			serverSocket = new DatagramSocket(port);
			this.serverAddress = InetAddress.getByName(serverAddress);
			this.serverPort = serverPort;
			setupClientListener();
			startClientListener();
			mitmUI = new MitMUI();
			mitmUI.display();
		} catch (SocketException e) {
			throw new PortUnavailableException("Cannot listen to port "+port+".");
		} catch (UnknownHostException e) {
			throw new InvalidServerAddressException("Unable to find server: "+serverAddress);
		}
	}
	
	public void send(String message){
		byte[] messageBuffer = message.getBytes();
		try {
			DatagramPacket packet = new DatagramPacket(messageBuffer, 
														messageBuffer.length, 
														serverAddress, serverPort);
			serverSocket.send(packet);
		} catch (IOException e){
			
		}
	}
	
	public void setupClientListener(){
		clientListener = new Thread(new Runnable(){
			@Override
			public void run() {
				alive = true;
				while(alive){
					byte[] buff = new byte[MAXIMUM_BUFFER_SIZE];
					DatagramPacket packet = new DatagramPacket(buff,buff.length);
					
					try {
						serverSocket.receive(packet);
						String data = new String(buff).trim();
						InetAddress sourceAddress = packet.getAddress();
						int sourcePort = packet.getPort();
						mitmUI.logStatus("Packet ["+data+"] was sent from ["+sourceAddress+":"+sourcePort+"] to "+serverAddress+":"+serverPort);
						packet.setAddress(serverAddress);
						packet.setPort(serverPort);
						
						serverSocket.send(packet);
						
						} catch (IOException e) {
						System.err.println("ERROR.");
						throw new PacketReceiveException("Server socket cannot receive packets!");
					}
				}
				
				if(serverSocket != null){
					serverSocket.close();
				}
			}
		});
	}
	
	public void startClientListener(){
		clientListener.start();
	}
}
