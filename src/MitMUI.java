import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MitMUI {
	private final JFrame frame;
	private final LoggerPanel logs;
	
	public MitMUI(){
		frame = new JFrame("SKIRMISH | Man in the Middle");
		logs = new LoggerPanel("Logs");
		init();
	}
	
	private void init(){
		setupCanvas();
		
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	}

	public void display(){
		frame.setVisible(true);
	}
	
	private void setupCanvas() {
		JPanel canvas = new JPanel();
		canvas.add(logs);
		
		frame.setContentPane(canvas);
	}
	
	public void logStatus(String status){
		logs.log(status);
	}
}
